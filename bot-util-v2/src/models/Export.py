import datetime
import io
import os
import typing
import json
import urllib
import zipfile

from src.models.BotIntent import BotIntent
from src.models.BotLocale import AwsBotLocaleDef, BotLocale

BOT_JSON = "Bot.json"
BOT_LOCALES = "BotLocales"
BOT_LOCALE = "BotLocale"
INTENT = "Intent"
SLOT_TYPE = "SlotType"
SLOT = "Slot"


class Export:
    """
    Represents the total export of a given bot. What used to be spread out amongst files and directories is now
    written in a single json file to make the import and storage of export related data easier and more manageable.
    """

    def __init__(self, bot_name, bot_id, bot_arn, bot_version, bot_export_id):
        self.name: str = bot_name
        self.id: str = bot_id
        self.arn: str = bot_arn
        self.version: str = bot_version
        self.export_id: str = bot_export_id
        self.export_url: str = None
        self.bot_def: typing.Dict = None
        self.aliases: typing.List = []
        self.locales: typing.Dict = {}

    @staticmethod
    def from_dict(export_dict):
        """Attempt to map a dict to an Export object."""
        try:
            bot_export = Export(
                export_dict['name'],
                export_dict['id'],
                export_dict['arn'],
                export_dict['version'],
                export_dict['export_id'],
            )
            for k, v in export_dict.items():
                if k in bot_export.__dict__.keys():
                    bot_export.__dict__[k] = v
            return bot_export
        except Exception as ex:
            raise ValueError(f'Error when mapping dict to export: {ex}')

    def fetch_bot_export(self) -> bool:
        """
        Fetch the export data if there is an export url assigned and map the data.
        :return: True if it succeeded, false else.
        """
        if self.export_url is None:
            raise ValueError("Export URL is missing. Unable to fetch data!")

        with urllib.request.urlopen(self.export_url) as r_zip:
            try:
                with zipfile.ZipFile(io.BytesIO(r_zip.read())) as zip_data:
                    zip_list = zip_data.infolist()
                    return self.prioritized_extract_data(zip_data, zip_list)
            except Exception as e:
                raise ValueError(f'Error when fetching bot export: {e}')

    def prioritized_extract_data(self, zip_data, path_list) -> bool:
        """ Scrub and prioritize the paths before extracting data to ensure things are built in the correct order."""
        _zlist = [fp for fp in path_list if BOT_LOCALE == str(fp.filename.split("/")[-1]).split(".")[0]]
        [_zlist.append(fp) for fp in path_list if BOT_JSON == str(fp.filename.split("/")[-1])]
        [_zlist.append(fp) for fp in path_list if INTENT == str(fp.filename.split("/")[-1]).split(".")[0]]
        [_zlist.append(fp) for fp in path_list if SLOT_TYPE == str(fp.filename.split("/")[-1]).split(".")[0]]
        [_zlist.append(fp) for fp in path_list if SLOT == str(fp.filename.split("/")[-1]).split(".")[0]]
        for z_file in _zlist:
            self.extract_data(z_file, zip_data)
        return True

    def extract_data(self, z_file, zip_data):
        """Iterate the zipped export and populate locales property by parsing the data using paths to map the data."""
        path_arr = z_file.filename.split("/")[1:]
        if len(path_arr) > 0:
            # store the json object as the value of the created property
            with zip_data.open(z_file) as curr_file:
                data = json.loads(curr_file.read().decode('utf-8'))
                # print(f'DEBUG: {path_arr}')
                try: # TODO: Refactor to object literal instead of nested ifs to keep it clean.
                    first_element = str(path_arr[0])
                    if BOT_JSON == first_element:
                        self.bot_def = data
                    if BOT_LOCALES == first_element:
                        last_element = str(path_arr[-1]).split(".")[0]
                        if BOT_LOCALE == last_element:
                            locale_name = path_arr[-2]
                            self.locales.update({locale_name: BotLocale.from_dict(data)})
                        if INTENT == last_element:
                            locale = path_arr[1]
                            intent_name = path_arr[3]
                            self.locales[locale].intents.update({intent_name: BotIntent.from_dict(data)})
                        if SLOT_TYPE == last_element:
                            slot_type = path_arr[-2]
                            locale = path_arr[1]
                            self.locales[locale].slot_types.update(
                                {slot_type: data})  # TODO: Add and map to SlotType Models
                        if SLOT == last_element:
                            slot_name = path_arr[-2]
                            locale = path_arr[1]
                            intent_name = path_arr[3]
                            self.locales[locale].intents[intent_name].slots.update({slot_name: data})
                except KeyError as ke:
                    raise KeyError(f"{ke} definition missing. Likely caused by order dependency during import")

    def write_to_disk(self, out_dir):
        """Write the export object to disk."""
        base_path = f'{out_dir}/{self.name}'
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        compiled_path = f'{base_path}.json'
        with open(compiled_path, 'wt') as out_file:
            out_file.write(self.__repr__())

    def __repr__(self):
        return json.dumps(self, indent=4, default=lambda o: o.__dict__ if not isinstance(o, datetime.date)
        else dict(year=o.year, month=o.month, day=o.day))
