import typing


class AwsBotLocaleDef:
    """This class represents the current AWS BotLocale API Definition, and should be mapped to when interacting with
    data to or from AWS."""
    def __init__(self):
        self.name: str = None
        self.identifier: str = None
        self.version: str = None
        self.description: str = None
        self.voiceSettings: typing.Dict = {}
        self.nluConfidenceThreshold: float = None

    @staticmethod
    def from_model(locale_model):
        """Striped domain data from the object and matches the current aws definition"""
        aws_bot_locale = AwsBotLocaleDef()
        for k, v in locale_model.__dict__.items():
            if k in aws_bot_locale.__dict__.keys():
                aws_bot_locale.__dict__[k] = v
        return aws_bot_locale


class BotLocale(AwsBotLocaleDef):
    """ This class extends the AWS definition of a bot locale so we can append domain specific properties to the object.
Each implementation of the model contains a mapping method to convert to and from the DTO and the API Defined mode."""
    def __init__(self):
        super().__init__()
        self.intents: typing.Dict = {}
        self.slot_types: typing.Dict = {}

    @staticmethod
    def from_dict(locale_dict):
        """Takes a standard python dict object and maps it to and object to perform operations on."""
        aws_bot_locale = BotLocale()
        for k, v in locale_dict.items():
            if k in aws_bot_locale.__dict__.keys():
                aws_bot_locale.__dict__[k] = v
        return aws_bot_locale

    @staticmethod
    def from_aws_def(aws_bot_locale_obj):
        """Takes in an AwsBotAliasDef object and maps it to a DTO"""
        bot_locale_dto = BotLocale()
        for k, v in aws_bot_locale_obj.__dict__.items():
            if k in bot_locale_dto.__dict__.keys():
                bot_locale_dto.__dict__[k] = v
        return bot_locale_dto
