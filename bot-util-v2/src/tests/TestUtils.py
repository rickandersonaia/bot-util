import zipfile
import json


class TestUtils:
    @staticmethod
    def load_zip(path):
        try:
            with zipfile.ZipFile(path) as zip_data:
                return zip_data
        except Exception as e:
            return None

    @staticmethod
    def load_json(path):
        try:
            with open(path) as json_data:
                return json.loads(json_data.read())
        except Exception as e:
            return None