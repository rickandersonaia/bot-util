import logging

from botocore.exceptions import ClientError

from src.models.BotLocale import BotLocale
from src.models.Export import Export


class LocaleManager:
    """Static helper class that encapsulates methods associated with locale management."""
    @staticmethod
    def try_create_locale(logger, lex_client, bot_import: Export, bot_id, locale_id, locale):
        """ Attempt to create a new locale with this id, updates if it already exists."""
        try:
            logger.info(f'Updating locale: {bot_import.name}::{locale_id}')
            response = lex_client.create_bot_locale(
                botId=bot_id,
                botVersion='DRAFT',
                localeId=locale_id,
                nluIntentConfidenceThreshold=locale['nluConfidenceThreshold'],
                voiceSettings=locale['voiceSettings']
            )
            logger.info(f'Creat locale response: {response}')
            return response['botLocaleStatus']
        except ClientError as ex:
            error_code = ex.response['Error']['Code']
            error_msg = ex.response['Error']['Message']
            if error_code == 'PreconditionFailedException':
                if f"Bot Locale with name {locale['name']} already exists." in error_msg:
                    return LocaleManager.update_locale(logger, lex_client, bot_import, bot_id, locale_id)

    @staticmethod
    def update_locale(logger: logging.Logger, lex_client, bot_import: Export, bot_id, locale_id, version='DRAFT'):
        """Update an existing locale"""
        desc = bot_import.locales[locale_id]['description']
        locale = bot_import.locales[locale_id]
        response = lex_client.update_bot_locale(
            botId=bot_id,
            botVersion=version,
            localeId=locale_id,
            description='' if desc is None else desc,
            nluIntentConfidenceThreshold=locale['nluConfidenceThreshold'],
            voiceSettings=locale['voiceSettings'])
        logger.debug(f'Update Locale Response : {response}')

    @staticmethod
    def update_locales(logger, lex_client, bot_id, bot):
        """Update the locales for the given bot. Attempts to create them first, and falls back to update if that fails."""
        for locale_id, locale in bot.locales.items():
            LocaleManager.try_create_locale(logger, lex_client, bot, bot_id, locale_id, locale)
